// Copyright (C) 2022 Storj Labs, Inc.
// See LICENSE for copying information.

package main

import "fmt"

type Segment struct {
	a int
	b int
	c int
}

func main() {
	n := int64(250000)
	segments := make([]Segment, n)

	d1 := observeSingle(NullSingleObserver{}, segments)
	d2 := observeSingle(NullSingleObserver2{}, segments)
	fmt.Printf("observeSingle %.2f ns %.2f ns\n",
		float64(d1.Nanoseconds())/float64(n),
		float64(d2.Nanoseconds())/float64(n),
	)

	total1, inner1 := observeSingleWithTime(NullSingleObserver{}, segments)
	total2, inner2 := observeSingleWithTime(NullSingleObserver2{}, segments)
	println("observeSingleWithTime total", total1.Nanoseconds()/n, "ns ", total2.Nanoseconds()/n, "ns")
	println("observeSingleWithTime inner", inner1.Nanoseconds()/n, "ns ", inner2.Nanoseconds()/n, "ns")

	d3 := observeMultiple(NullMultiObserver{}, segments)
	d4 := observeMultiple(NullMultiObserver2{}, segments)

	fmt.Printf("observeMultiple %.2f ns %.2f ns\n",
		float64(d3.Nanoseconds())/float64(n),
		float64(d4.Nanoseconds())/float64(n),
	)
}
