// Copyright (C) 2022 Storj Labs, Inc.
// See LICENSE for copying information.

package main

import "time"

type MultiObserver interface {
	Process([]Segment)
}

type NullMultiObserver struct {
	count int
}

func (n NullMultiObserver) Process(segments []Segment) {
	for _, s := range segments {
		// call a non-virtual function so the comparison is kinda fair
		n.Add(&s)
	}
}

func (n NullMultiObserver) Add(segment *Segment) {
	// do an op so the optimizer doesn't elide this function
	n.count += segment.a
}

type NullMultiObserver2 struct {
	count int
}

func (n NullMultiObserver2) Process(segments []Segment) {
	for _, s := range segments {
		// call a non-virtual function so the comparison is kinda fair
		n.Add(&s)
	}
}

func (n NullMultiObserver2) Add(segment *Segment) {
	// do an op so the optimizer doesn't elide this function
	n.count += segment.a
}

func observeMultiple(observer MultiObserver, segments []Segment) time.Duration {
	start := time.Now()

	observer.Process(segments)

	return time.Since(start)
}
