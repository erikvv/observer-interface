// Copyright (C) 2022 Storj Labs, Inc.
// See LICENSE for copying information.

package main

import "time"

type SingleObserver interface {
	Process(*Segment)
}

type NullSingleObserver struct {
}

func (n NullSingleObserver) Process(s *Segment) {
}

type NullSingleObserver2 struct {
}

func (n NullSingleObserver2) Process(s *Segment) {
}

func observeSingle(observer SingleObserver, segments []Segment) time.Duration {
	start := time.Now()
	for _, s := range segments {
		observer.Process(&s)
	}
	return time.Since(start)
}

func observeSingleWithTime(observer SingleObserver, segments []Segment) (total time.Duration, inner time.Duration) {
	start := time.Now()

	var duration time.Duration = 0
	for _, s := range segments {
		start := time.Now()
		observer.Process(&s)
		duration = duration + time.Since(start)
	}

	return time.Since(start), duration
}
