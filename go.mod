module evanv.nl/segmentloop

go 1.19

require storj.io/common v0.0.0-20221101132628-f2f1c7a50597

require (
	github.com/zeebo/errs v1.3.0 // indirect
	storj.io/drpc v0.0.32 // indirect
)
