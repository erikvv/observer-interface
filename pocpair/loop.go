// Copyright (C) 2022 Storj Labs, Inc.
// See LICENSE for copying information.

package main

import (
	"context"
	"time"

	"evanv.nl/segmentloop"

	"storj.io/common/errs2"
)

type Loop struct {
	Parellelism int
	Observers   []Observer
}

type observerState struct {
	observer       Observer
	rangeObservers []RangeObserver
	duration       time.Duration
}

type RangeState struct {
	ranges   RangeObserver
	duration time.Duration
}

func (l Loop) RunOnce(ctx context.Context) error {
	loopStartInfo := segmentloop.LoopStartInfo{}
	observerStates := []observerState{}

	for _, obs := range l.Observers {
		err := obs.LoopStarted(ctx, loopStartInfo)
		if err != nil {
			return err
		}

		ranges := []RangeObserver{}
		for i := 0; i <= l.Parellelism; i++ {
			rangeObserver, err2 := obs.CreateRangeObserver(ctx)
			if err2 != nil {
				return err2
			}
			ranges = append(ranges, rangeObserver)
		}

		observerStates = append(observerStates, observerState{
			observer:       obs,
			rangeObservers: ranges,
		})
	}

	group := errs2.Group{}
	for i := 0; i < l.Parellelism; i++ {
		// don't want to capture i in the closure
		j := i
		group.Go(func() error {
			provider := segmentloop.SegmentProvider{}
			rangeObservers := []RangeObserver{}

			for _, state := range observerStates {
				// for every observer, get every range allocated for this worker
				rangeObservers = append(rangeObservers, state.rangeObservers[j])
			}

			for segments := provider.GetBatch(); segments != nil; segments = provider.GetBatch() {
				for _, rangeObserver := range rangeObservers {
					for _, segment := range segments {
						err := rangeObserver.Segment(ctx, &segment)
						if err != nil {
							return err
						}
					}
				}
			}

			return nil
		})
	}

	errs := group.Wait()
	if errs != nil {
		return errs[0]
	}

	for _, state := range observerStates {
		for _, rangeObserver := range state.rangeObservers {
			err := rangeObserver.RangeEnded(ctx, state.observer)
			if err != nil {
				return err
			}
		}

		err := state.observer.LoopEnded(ctx, segmentloop.LoopEndInfo{})
		if err != nil {
			return err
		}
	}

	return nil
}
