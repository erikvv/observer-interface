// Copyright (C) 2022 Storj Labs, Inc.
// See LICENSE for copying information.

package main

import (
	"context"
	"fmt"

	"evanv.nl/segmentloop"
)

type Observer interface {
	LoopStarted(context.Context, segmentloop.LoopStartInfo) error
	CreateRangeObserver(context.Context) (RangeObserver, error)
	// Called after all RangeObserver have had the opportunity to pass their data to the main Observer
	LoopEnded(context.Context, segmentloop.LoopEndInfo) error
}

type RangeObserver interface {
	RangeStarted(context.Context, segmentloop.LoopStartInfo) error
	// will not be called concurrently on the same instance
	// (unless implementer choses to always return the same instance from CreateRangedObserver)
	Segment(context.Context, *segmentloop.Segment) error
	// opportunity to close resources and pass data to the main observer
	RangeEnded(context context.Context, parent Observer) error
}

type CountObserver struct {
	count int
}

func (c *CountObserver) LoopStarted(context.Context, segmentloop.LoopStartInfo) error {
	return nil
}
func (c *CountObserver) LoopEnded(context.Context, segmentloop.LoopEndInfo) error {
	return nil
}

func (c *CountObserver) CreateRangeObserver(context.Context) (RangeObserver, error) {
	return &CountRangeObserver{}, nil
}

type CountRangeObserver struct {
	count int
}

func (c *CountRangeObserver) RangeStarted(context.Context, segmentloop.LoopStartInfo) error {
	return nil
}

func (c *CountRangeObserver) Segment(context.Context, *segmentloop.Segment) error {
	c.count++
	return nil
}

func (c *CountRangeObserver) RangeEnded(context context.Context, parent Observer) error {
	countObserver, ok := parent.(*CountObserver)
	if !ok {
		return fmt.Errorf("parent is of invalid type")
	}
	countObserver.count += c.count
	return nil
}
