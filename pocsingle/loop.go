package main

import (
	"context"
	"time"

	"evanv.nl/segmentloop"

	"storj.io/common/errs2"
)

type Loop struct {
	Parellelism int
	Observers   []Observer
}

type observerState struct {
	observer Observer
	duration time.Duration
}

func (l Loop) RunOnce(ctx context.Context) error {
	loopStartInfo := segmentloop.LoopStartInfo{}
	observerStates := []observerState{}

	for _, obs := range l.Observers {
		err := obs.LoopStarted(ctx, loopStartInfo)
		if err != nil {
			return err
		}

		observerStates = append(observerStates, observerState{
			observer: obs,
		})
	}

	group := errs2.Group{}
	for i := 0; i < l.Parellelism; i++ {
		group.Go(func() error {
			provider := segmentloop.SegmentProvider{}
			for segments := provider.GetBatch(); segments != nil; segments = provider.GetBatch() {
				for _, obs := range l.Observers {
					err := obs.Segment(ctx, segments)
					if err != nil {
						return err
					}
				}
			}
			return nil
		})
	}

	errs := group.Wait()
	if errs != nil {
		return errs[0]
	}

	for _, obs := range l.Observers {
		err := obs.LoopEnded(ctx, segmentloop.LoopEndInfo{})
		if err != nil {
			return err
		}
	}

	return nil
}
