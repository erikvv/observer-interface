package main

import (
	"context"
	"fmt"
)

func main() {
	println("pocsingle")

	ctx := context.Background()

	countObserver := CountObserver{}

	loop := Loop{
		Parellelism: 16,
		Observers: []Observer{
			&countObserver,
		},
	}

	err := loop.RunOnce(ctx)
	if err != nil {
		println(err.Error())
	}

	fmt.Println("Expected count: ", loop.Parellelism*4*2500)
	fmt.Println("Count: ", countObserver.count)
}
