package main

import (
	"context"
	"sync"

	"evanv.nl/segmentloop"
)

type Observer interface {
	LoopStarted(context.Context, segmentloop.LoopStartInfo) error
	Segment(context.Context, []segmentloop.Segment) error
	// Called after all RangeObserver have had the opportunity to pass their data to the main Observer
	LoopEnded(context.Context, segmentloop.LoopEndInfo) error
}

type CountObserver struct {
	mu    sync.Mutex
	count int
}

func (obs *CountObserver) LoopStarted(context.Context, segmentloop.LoopStartInfo) error {
	return nil
}
func (obs *CountObserver) LoopEnded(context.Context, segmentloop.LoopEndInfo) error {
	return nil
}
func (obs *CountObserver) Segment(ctx context.Context, segments []segmentloop.Segment) error {
	intermediateCount := 0
	for range segments {
		intermediateCount += 1
	}

	obs.mu.Lock()
	defer obs.mu.Unlock()
	obs.count += intermediateCount
	return nil
}
