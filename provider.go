// Copyright (C) 2022 Storj Labs, Inc.
// See LICENSE for copying information.

package segmentloop

type LoopStartInfo struct {
}

type LoopEndInfo struct{}

type Segment struct{}

type SegmentProvider struct {
	nBatches int
}

func (s *SegmentProvider) GetBatch() []Segment {
	if s.nBatches >= 4 {
		return nil
	}

	s.nBatches += 1

	return make([]Segment, 2500)
}
